package practica2;

import java.util.Scanner;

/**
 *
 * @author m_fer
 */
public class Practica2 {

    public static void main(String[] args) {
        
        //Para las dimensiones
        int dims[][] = new int[2][2]; //Guarda las dimensiones de las matrices ya como enteros
        String dimString[] = new String[2]; //Arreglo de String para ingresar dimensiones con la forma nxm
        String auxDim[]; //Auxiliar de dimString para separar las dimensiones
        boolean aux = false;
        
        //Para las matrices
        String mA[][];
        String mAaux[][];
        String mB[][];
        String mBaux[][];
        
        int matrizA[][];        
        int matrizB[][];
        int matrizR[][];
        
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < 2; i++) {
            do {      
                //Pedimos las dimensiones
                System.out.print("Ingresa la dimension para la matriz " + (i+1) + ": ");
                dimString[i] = sc.nextLine(); //Recibe las dimensiones en tipo String y de la forma nxm
                
                try {
                    //Transforma las dimensiones
                    auxDim = dimString[i].split("x", 2); //Separa las dimensiones nxm en n,m
                    for (int j = 0; j < 2; j++) {
                        dims[i][j] = Integer.parseInt(auxDim[j]); //Transforma las dimensiones n,m en enteros
                    }
                    aux = false;
                } catch (Exception e) {
                    System.out.println("Ingresa un formato valido para las matrices... (2x2)");
                    aux = true;
                }
                
            } while (aux);
        }
            //FilasA   == ColumnasB
        if (dims[0][1] == dims[1][0]) { 
            
            System.out.println("\nSe puede multiplicar la matriz\n");
                               //FilasA   ColumnasA
            matrizA = new int[dims[0][0]][dims[0][1]];
                               //FilasB   ColumnasB
            matrizB = new int[dims[1][0]][dims[1][1]];
            //Matriz Resultado con dimension 
                               //FilasA   ColumnasB
            matrizR = new int[dims[0][0]][dims[1][1]];
            
            //Pedir las matrices
            mA = new String[dims[0][0]][1];
            mAaux = new String[dims[0][0]][dims[0][1]];
            mB = new String[dims[1][0]][1];
            mBaux = new String[dims[1][0]][dims[1][1]];
            
            //Pedir matriz a
                                //FilasA
            for (int i = 0; i < dims[0][0]; i++) {
                do {
                    System.out.print("Para la matriz A, ingresa la fila " + (i+1) + ": ");
                    mA[i][0] = sc.nextLine(); //Se lee toda una fila en tipo String "a1 a2 a3...an"
                    try {
                        mAaux[i] = mA[i][0].split(" ", dims[0][1]); // Separa los valores ingresados "a1,a2,a3,...,an"
                                        //ColumnasA
                        for (int j = 0; j < dims[0][1]; j++) {
                            //Convierte los valores a entero y se asignan al espacio correspondiente de la matrizA
                            matrizA[i][j] = Integer.parseInt(mAaux[i][j]);
                        }
                        aux = false;
                    } catch (Exception e) {
                        System.out.println("Ingresa un formato vaido para la fila... (1 2 3)");
                        aux = true;
                    }
                } while (aux);
            }
            System.out.println("");
            
            //Pedir matriz b
                                //FilasB
            for (int i = 0; i < dims[1][0]; i++) {
                do {
                    System.out.print("Para la matriz B, ingresa la fila " + (i+1) + ": ");
                    mB[i][0] = sc.nextLine();//Se lee toda una fila en tipo String "b1 b2 b3...bn"
                    try {
                        mBaux[i] = mB[i][0].split(" ", dims[1][1]); // Separa los valores ingresados "b1,b2,b3,...,bn"
                                           //ColumnasB
                        for (int j = 0; j < dims[1][1]; j++) {
                            //Convierte los valores a entero y se asignan al espacio correspondiente de la matrizB
                            matrizB[i][j] = Integer.parseInt(mBaux[i][j]);
                        }
                        aux = false;
                    } catch (Exception e) {
                        System.out.println("Ingresa un formato vaido para la fila... (1 2 3)");
                        aux = true;
                    }
                } while (aux);
            }
            System.out.println("");
            // Multiplicación de matrices
                   //dims[0][1] = ColumnasA = FilasB = 3
            for (int i = 0; i < dims[0][1]; i++) { //Recorre las Columnas de A y las Filas de B
                                   //FilasA = 4
                for (int j = 0; j < dims[0][0]; j++) { //Recorre las Filas de A y las Filas de R
                                       //ColumnasB = 4
                    for (int k = 0; k < dims[1][1]; k++) { //Recorre las Columnas de R y las Columnas de B
                        
                        //Vuelta 1                              //Vuelta 2                              //Vuelta3                              V1     V2    V3
                        //AB[0][0] += A[0][0] * B[0][0];        //AB[0][0] += A[0][1] * B[1][0];        //AB[0][0] += A[0][2] * B[2][0];    = 10*5 + 8*1 + 15*3 = 103
                        //AB[0][1] += A[0][0] * B[0][1];        //AB[0][1] += A[0][1] * B[1][1];        //AB[0][1] += A[0][2] * B[2][1];    = 10*5 + 8*2 + 15*1 = 81
                        //AB[0][2] += A[0][0] * B[0][2];        //AB[0][2] += A[0][1] * B[1][2];        //AB[0][2] += A[0][2] * B[2][2];    = 10*5 + 8*4 + 15*7 = 187
                        //AB[0][3] += A[0][0] * B[0][3];        //AB[0][3] += A[0][1] * B[1][3];        //AB[0][3] += A[0][2] * B[2][3];    = 10*3 + 8*2 + 15*0 = 46
                        
                        //AB[1][0] += A[1][0] * B[0][0];        //AB[1][0] += A[1][1] * B[1][0];        //AB[1][0] += A[1][2] * B[2][0];    = 5*5 + 2*1 + 10*3 = 57
                        //AB[1][1] += A[1][0] * B[0][1];        //AB[1][1] += A[1][1] * B[1][1];        //AB[1][1] += A[1][2] * B[2][1];    = 5*5 + 2*2 + 10*1 = 39
                        //AB[1][2] += A[1][0] * B[0][2];        //AB[1][2] += A[1][1] * B[1][2];        //AB[1][2] += A[1][2] * B[2][2];    = 5*5 + 2*4 + 10*7 = 103
                        //AB[1][3] += A[1][0] * B[0][3];        //AB[1][3] += A[1][1] * B[1][3];        //AB[1][3] += A[1][2] * B[2][3];    = 5*3 + 2*2 + 10*0 = 19
                        
                        //AB[2][0] += A[2][0] * B[0][0];        //AB[2][0] += A[2][1] * B[1][0];        //AB[2][0] += A[2][2] * B[2][0];    = 20*5 + 7*1 + 2*3 = 113
                        //AB[2][1] += A[2][0] * B[0][1];        //AB[2][1] += A[2][1] * B[1][1];        //AB[2][1] += A[2][2] * B[2][1];    = 20*5 + 7*2 + 2*1 = 116
                        //AB[2][2] += A[2][0] * B[0][2];        //AB[2][2] += A[2][1] * B[1][2];        //AB[2][2] += A[2][2] * B[2][2];    = 20*5 + 7*4 + 2*7 = 142
                        //AB[2][3] += A[2][0] * B[0][3];        //AB[2][3] += A[2][1] * B[1][3];        //AB[2][3] += A[2][2] * B[2][3];    = 20*3 + 7*2 + 2*0 = 74
                        
                        //AB[3][0] += A[3][0] * B[0][0];        //AB[3][0] += A[3][1] * B[1][0];        //AB[3][0] += A[3][2] * B[2][0];    = 1*5 + 4*1 + 3*3 = 18
                        //AB[3][1] += A[3][0] * B[0][1];        //AB[3][1] += A[3][1] * B[1][1];        //AB[3][1] += A[3][2] * B[2][1];    = 1*5 + 4*2 + 3*1 = 16
                        //AB[3][2] += A[3][0] * B[0][2];        //AB[3][2] += A[3][1] * B[1][2];        //AB[3][2] += A[3][2] * B[2][2];    = 1*5 + 4*4 + 3*7 = 42
                        //AB[3][3] += A[3][0] * B[0][3];        //AB[3][3] += A[3][1] * B[1][3];        //AB[3][3] += A[3][2] * B[2][3];    = 1*3 + 4*2 + 3*0 = 11
                       
                        matrizR[j][k] += matrizA[j][i] * matrizB[i][k];
                    }
                }
            }
                                //FilasA
            for (int i = 0; i < dims[0][0]; i++) { //recorre las filas de la matriz resultante
                                    //ColumnasB
                for (int j = 0; j < dims[1][1]; j++) { //recorre las columnas de la matriz resultante
                    //Imprime cada posición de la matriz resultante
                    System.out.print(matrizR[i][j] + " ");
                }
                System.out.println(" ");
            }
        } else {
            System.out.println("\nNo se puede multiplicar la matriz\n");
        }
    }
    
}
